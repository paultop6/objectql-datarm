import typing

from inspect import isclass
from typing import List, Union, Dict, Type

from graphql import GraphQLObjectType, GraphQLString
from graphql.type.definition import \
    GraphQLType, \
    GraphQLField, \
    GraphQLList, \
    GraphQLInputObjectField, \
    define_field_map, \
    GraphQLScalarType, \
    GraphQLNonNull


try:
    from sqlalchemy import inspect as sqlalchemy_inspect, Column
    from sqlalchemy.databases import postgres
    from sqlalchemy.ext.associationproxy import AssociationProxy
    from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
    from sqlalchemy.orm import interfaces, RelationshipProperty, relationship
    from sqlalchemy.sql.type_api import TypeEngine

except ImportError:
    raise ImportError("SQLAlchemy package not found")


from objectql.mapper import ObjectQLTypeWrapper, ObjectQLTypeMapper
from objectql.types import GraphQLUUID
from objectql.utils import to_camel_case


class HybridWrapper:

    def __init__(self, prop):
        self.property = prop


class AssociationWrapper:

    def __init__(self, prop, key, parent):
        self.property = prop
        self.key = key
        self.parent = parent


class ObjectQLSQLAlchemyMixin(ObjectQLTypeWrapper):
    """
    `ObjectQLSQLAlchemyMixin` subclasses will map
    the following into properties on a ObjectQLObject

        - Columns
        - Relationships
        - Hybrid Properties
        - Hybrid Methods
        - Association Objects

    This mixin will exclude private fields
    (field names that begin with an underscore),
    and exclude any fields that are listed in 'graphql_exclude_fields'
    """

    @classmethod
    def graphql_merge(cls, cls_):
        for key, member in cls_.__dict__.items():
            if hasattr(member, 'graphql'):
                setattr(cls, key, member)

    @classmethod
    def graphql_exclude_fields(cls) -> List[str]:
        return []

    # noinspection PyPackageRequirements
    @classmethod
    def graphql_type(cls, mapper: ObjectQLTypeMapper) -> GraphQLObjectType:
        base_type: GraphQLObjectType = mapper.map(cls, use_graphql_type=False)

        # Remove any modifiers
        while hasattr(base_type, 'of_type'):
            base_type = base_type.of_type

        if mapper.as_input:
            return base_type

        properties_type = Dict[
            str,
            Union[Column, RelationshipProperty, HybridWrapper]
        ]

        properties: properties_type = {}
        inspected_model = sqlalchemy_inspect(cls)

        for name, item in inspected_model.columns.items():
            properties[name] = item

        for _relationship in inspected_model.relationships:
            properties[_relationship.key] = _relationship

        for name, item in inspected_model.all_orm_descriptors.items():
            if isinstance(item, (hybrid_method, hybrid_property)):
                properties[name] = HybridWrapper(getattr(cls, name))

            if isinstance(item, AssociationProxy):
                properties[name] = AssociationWrapper(
                    getattr(cls, name),
                    name,
                    cls
                )

        exclude_fields = cls.graphql_exclude_fields()

        properties = {
            name: prop
            for name, prop in properties.items()
            if not name.startswith("_") and name not in exclude_fields
        }

        def local_fields_callback():
            local_type = base_type
            local_properties = properties
            local_mapper = mapper

            # noinspection PyProtectedMember
            local_type_fields = local_type._fields

            def fields_callback():
                local_fields = {}

                if local_type_fields:
                    try:
                        local_fields = define_field_map(
                            local_type,
                            local_type_fields
                        )
                    except AssertionError:
                        pass

                for prop_name, prop in local_properties.items():

                    def local_resolver():
                        local_prop_name = prop_name

                        def resolver(
                            self,
                            info=None,
                            context=None,
                            *args,
                            **kwargs
                        ):
                            return getattr(self, local_prop_name)
                        return resolver

                    type_: GraphQLType = ObjectQLSQLAlchemyHelpers.map(
                        prop,
                        local_mapper
                    )

                    if local_mapper.as_input:
                        field = GraphQLInputObjectField(type_)
                    else:
                        if hasattr(prop, 'nullable') and not prop.nullable:
                            type_ = GraphQLNonNull(type_)

                        field = GraphQLField(type_, resolver=local_resolver())

                    local_fields[to_camel_case(prop_name)] = field

                return local_fields

            return fields_callback

        base_type._fields = local_fields_callback()

        return base_type


prop_types = Union[
    Column,
    RelationshipProperty,
    HybridWrapper,
    AssociationWrapper
]


class ObjectQLSQLAlchemyHelpers:

    @staticmethod
    def map(
        prop: prop_types,
        mapper: ObjectQLTypeMapper
    ) -> GraphQLType:
        if isinstance(prop, Column):
            return ObjectQLSQLAlchemyHelpers.map_column(prop.type, mapper)

        elif isinstance(prop, RelationshipProperty):
            return ObjectQLSQLAlchemyHelpers.map_relationship(prop, mapper)

        elif isinstance(prop, HybridWrapper):
            return ObjectQLSQLAlchemyHelpers.map_hybrid(prop, mapper)

        elif isinstance(prop, AssociationWrapper):
            return ObjectQLSQLAlchemyHelpers.map_association(prop, mapper)

        return GraphQLString

    @staticmethod
    def map_column(
        column_type: Column,
        mapper: ObjectQLTypeMapper
    ) -> GraphQLType:
        try:
            python_type = column_type.python_type
        except NotImplementedError:
            if isclass(column_type):
                column_type_class = column_type
            else:
                column_type_class = type(column_type)

            if issubclass(column_type_class, TypeEngine):
                return ObjectQLSQLAlchemyHelpers.map_type(column_type_class)
        else:
            return mapper.map(python_type)

    @staticmethod
    def map_type(type_: Type[TypeEngine]) -> GraphQLScalarType:
        scalar_map = [
            ([postgres.UUID], GraphQLUUID),
            ([postgres.ENUM], GraphQLString)
        ]

        for test_types, graphql_type in scalar_map:
            for test_type in test_types:
                if issubclass(type_, test_type):
                    return graphql_type

    @staticmethod
    def map_hybrid(
        hybrid_type: HybridWrapper,
        mapper: ObjectQLTypeMapper
    ) -> GraphQLScalarType:

        type_hints = typing.get_type_hints(hybrid_type.property)
        scalar_type: GraphQLScalarType = mapper.map(
            type_hints.pop('return', None)
        )
        return scalar_type

    @staticmethod
    def map_association(
        association_type: AssociationWrapper,
        mapper: ObjectQLTypeMapper
    ) -> GraphQLType:

        association_proxy: AssociationProxy = association_type.property
        target_relationship: relationship = getattr(
            association_type.parent,
            association_proxy.target_collection
        )
        target_class = target_relationship.mapper.entity

        association_target_property = getattr(
            target_class,
            association_proxy.value_attr
        )

        column = association_target_property.property.columns[0]
        scalar_type: GraphQLScalarType = ObjectQLSQLAlchemyHelpers.map(
            column,
            mapper
        )

        return GraphQLList(scalar_type)

    @staticmethod
    def map_relationship(
        relationship: RelationshipProperty,
        mapper: ObjectQLTypeMapper
    ) -> GraphQLType:

        direction = relationship.direction
        model = relationship.mapper.entity

        graphql_type = mapper.map(model)
        if graphql_type:
            if not direction:
                return GraphQLList(graphql_type)
            if direction == interfaces.MANYTOONE or not relationship.uselist:
                return graphql_type
            elif direction in (interfaces.ONETOMANY, interfaces.MANYTOMANY):
                return GraphQLList(graphql_type)
