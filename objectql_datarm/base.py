from uuid import UUID

from typing import Optional

from sqlalchemy.orm.exc import NoResultFound

from objectql import abstract
from objectql.relay import Node

from objectql_datarm.sqlalchemy import ObjectQLSQLAlchemyMixin


@abstract
class ObjectQLDataRMRelayBase(Node, ObjectQLSQLAlchemyMixin):

    @classmethod
    def graphql_from_input(cls, id: UUID):
        value = cls.get(id)

        if value is None:
            raise NoResultFound(
                f"{cls.__name__} with UUID {id} was not found."
            )

    @classmethod
    def get(cls, id: UUID = None) -> Optional['Base']:
        if id:
            return cls.filter(id=id).one_or_none()
